import { useEffect, useState } from 'react';
import './App.css';
import Guest from './components/Guest/Guest';
import { guestList } from './guest.data';
import { IGuest } from './guest.data';

const DiwaliPartyApp = () => {

  const [Guests, setGuests] = useState<IGuest[]>([]);
  const [Maybe, setMaybe] = useState<IGuest[]>([]);
  const [Invited, setInvited] = useState<IGuest[]>([]);

  useEffect(() => {
    setGuests(guestList);
  }, []);

  const inviteGuest = (guestId: number) => {
    const GuestClone = [...Guests];
    let InvitedClone = [...Invited]
    const filterInvited  = GuestClone.filter((guest) => guest.id === guestId)
    const filterGuestClone = GuestClone.filter((guest) => guest.id !== guestId);
    InvitedClone = [...InvitedClone,...filterInvited];
    setInvited(InvitedClone);
    setGuests(filterGuestClone);
  }
  const invitedMaybe = (guestId: number) => {
    const MaybeClone = [...Maybe];
    let InvitedClone = [...Invited]
    const filterInvited  = MaybeClone.filter((guest) => guest.id === guestId)
    const filterMaybeClone = MaybeClone.filter((guest) => guest.id !== guestId);
    InvitedClone = [...InvitedClone,...filterInvited];
    setInvited(InvitedClone);
    setMaybe(filterMaybeClone);
  }
  const maybeGuest = (guestId: number) => {
    const GuestClone = [...Guests];
    let MaybeClone = [...Maybe]
    const filterMaybe  = GuestClone.filter((guest) => guest.id === guestId)
    const filterGuestClone = GuestClone.filter((guest) => guest.id !== guestId);
    MaybeClone = [...MaybeClone,...filterMaybe];
    setMaybe(MaybeClone);
    setGuests(filterGuestClone);
  }
  const Uninvite = (guestId: number) => {
    let MaybeClone = [...Maybe];
    let GuestClone = [...Guests]
    const filterUnvited  = MaybeClone.filter((guest) => guest.id === guestId)
    const filterMaybeClone = MaybeClone.filter((guest) => guest.id !== guestId);
    MaybeClone = [...filterMaybeClone];
    GuestClone = [...GuestClone, ...filterUnvited]
    setMaybe(MaybeClone);
    setGuests(GuestClone);
  }
  const Invitee2Maybe = (guestId: number) => {
    const InvitedClone = [...Invited];
    let MaybeClone = [...Maybe]
    const filterMaybe  = InvitedClone.filter((guest) => guest.id === guestId);
    const filterInvitedClone = InvitedClone.filter((guest) => guest.id !== guestId);
    MaybeClone = [...MaybeClone,...filterMaybe];
    setMaybe(MaybeClone);
    setInvited(filterInvitedClone);
  }
  const Invitee2Uninvite = (guestId: number) => {
    const InvitedClone = [...Invited];
    let GuestClone = [...Guests];
    const filterGuest  = InvitedClone.filter((guest) => guest.id === guestId);
    const filterInvitedClone = InvitedClone.filter((guest) => guest.id !== guestId);
    GuestClone = [...GuestClone,...filterGuest];
    setGuests(GuestClone);
    setInvited(filterInvitedClone);
  }
  return (
    <>
      <h1>Diwali Party Invitation Portal</h1>
      <div className="main-container">
        <Guest
          title="Potential Initees"
          guestList={Guests}
          btn1="Invite"
          btn2="Maybe"
          fun1={inviteGuest}
          fun2={maybeGuest}
        />
        <Guest
          title="Maybe Invitees"
          guestList={Maybe}
          btn1="Invite"
          btn2="Uninvite"
          fun1={invitedMaybe}
          fun2={Uninvite}
        />
        <Guest
          title="Invitees"
          guestList={Invited}
          btn1="Maybe"
          btn2="Uninvite"
          fun1={Invitee2Maybe}
          fun2={Invitee2Uninvite}
        />
      </div>
    </>
  );
}

export default DiwaliPartyApp;
