import { IButtonProps } from "./Button.types";

const Button = ({ onClick, btnName }: IButtonProps) => (
  <button onClick={onClick}>{btnName}</button>
);

export default Button;