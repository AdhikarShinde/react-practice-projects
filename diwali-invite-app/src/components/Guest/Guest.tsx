import { IGuestListProps } from "./GuestList.types";
import './Guest.css';
import Button from "../Button/Button";


const Guest = ({ title, guestList, btn1, btn2, fun1, fun2 }: IGuestListProps) => {
    return <div>
        <h3>{title}</h3>
        <div className="guestContainer"><p>Name</p> <p>Actions</p></div>
        {
            guestList.map(guest => (
                <div className="guestContainer" key={guest.id}>
                    <p>{guest.name}</p>
                    <div>
                        <Button onClick={() => fun1(guest.id)} btnName={btn1} />
                        <Button onClick={() =>fun2(guest.id)} btnName={btn2} />
                    </div>
                </div>
            ))
        }
    </div>
}

export default Guest;