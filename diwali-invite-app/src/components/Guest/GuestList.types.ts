import { IGuest } from "../../guest.data";

export interface IGuestListProps {
    title: string;
    guestList: IGuest[];
    btn1: string;
    btn2: string;
    fun1: Function;
    fun2: Function;
}