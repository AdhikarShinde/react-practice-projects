export interface IGuest {
    id: number,
    name: string,
    status: number
}

export const guestList: IGuest[] = [
    {
        id: 1,
        name: "Henry Cavill",
        status: 2
    },
    {
        id: 2,
        name: "Grant Gustin",
        status: 2
    },
    {
        id: 3,
        name: "Kakarot(Guko)",
        status: 2
    },
    {
        id: 4,
        name: "Vincenzo Casssano",
        status: 2
    },
    {
        id: 5,
        name: "Monkey D Luffy",
        status: 2
    },
];