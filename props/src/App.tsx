import { useState } from 'react';
import './App.scss';
import Section from './components/Section/Section';

const App = () => {

  const section1 = useState(0);
  const [section1ClickCount, updateSection1] = section1 ;

  const section2 = useState(0);
  const [section2ClickCount, updateSection2] = section2 ;
  // NOT STATE VARIABLES

  const onClickHandlerSection1 = () => {
    const newValue = section1ClickCount + 1;
    updateSection1(newValue);
    // how to tell  react to update the UI (how to rerender this component?)
    console.log('S1', section1ClickCount);
  }

  const onClickHandlerSection2 = () => {
    const newValue = section2ClickCount + 1;
    updateSection2(newValue);
    // how to tell  react to update the UI (how to rerender this component?)
    console.log('S2', section2ClickCount);
  }

  return (
    <div>
      <h1>Props</h1>
      <main>
        <Section
          title="section 1"
          count={section2ClickCount}
          onButtonClick={onClickHandlerSection1}
        />

        <Section
          title="section 2"
          count={section1ClickCount}
          onButtonClick={onClickHandlerSection2}
        />
      </main>

      <footer>
        {/* the leaked style from the button component is applied to the p tag */}
        {/* to stop the style from leaking to the global space, we use modular scss */}
        {/* change the name of ths stylesheet to <name_of_style>.module.scss */}
        <p className="button">This is a p tag</p>
      </footer>
    </div>
  )
};

export default App;
