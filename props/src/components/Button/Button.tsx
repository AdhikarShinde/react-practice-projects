import styles from './Button.module.scss';
import { IButtonProps } from './Button.types';

const Button = (
    props: IButtonProps
) => <button onClick={props.onClick} className={styles.button}>{props.text}</button>;

export default Button;