import { MouseEventHandler } from "react";

export interface IButtonProps {
    onClick?: MouseEventHandler<HTMLButtonElement> | undefined;
    text?: string;
}