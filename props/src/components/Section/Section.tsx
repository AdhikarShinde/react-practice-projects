import Button from '../Button/Button';
import styles from './Section.module.scss';
import { ISectionProps } from './Section.types';

const Section = ({ title, count, onButtonClick }: ISectionProps) => {
    // console.log(props);
    // destructuring props (props is an OBJECT of type ISectionProps)
    // const { title, count, onButtonClick } = props; 
    return <div className={styles.section}>
        <h3>{title}</h3>
        <p>the other section is clicked {count} times</p>
        <Button 
            text="click"
            onClick={onButtonClick}
        />
    </div>
}

export default Section;