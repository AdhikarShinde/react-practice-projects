import { MouseEventHandler } from "react";

export interface ISectionProps {
    title: string;
    count: number | string;
    onButtonClick:  MouseEventHandler<HTMLButtonElement>;
}